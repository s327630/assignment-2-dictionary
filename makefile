ASM=nasm
ASMFLAGS=-f elf64
LD=ld

all: main

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o $@ $^

dict.o: dict.asm
	$(ASM) $(ASMFLAGS) -o $@ $^

main.o: main.asm lib.inc
	$(ASM) $(ASMFLAGS) -o $@ main.asm
        
main: main.o dict.o lib.o
	$(LD) -o $@ $^

.PHONY: clean
clean:
	$(RM) *.o
