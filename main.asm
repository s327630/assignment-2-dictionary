%include "lib.inc"
%include "words.inc"
%define BUFFER_SIZE 255

section .bss
    buffer: times BUFFER_SIZE db 0x0

section .rodata
    input_err_msg: db "Error: string is so big"
    find_err_msg: db "Error: key does not exist"


section .text
global _start
_start:
    mov rdi, buffer		
    mov rsi, [buffer_size]
    call read_word		; записываем пльзовательский ввод в буфер
    
    cmp rax, 0			; проверяем получилось ли заполнить буфер
    je .input_error
    
    push rdx			;сохраняем  длинну строки
    mov rdi, buffer
    mov rsi, first_word
    call find_word
    pop rdx
    
    cmp rax, 0
    je .find_error
    
    lea rdi, [rax+rdx+9] 	; находим адрес указателя на строку значения
    call print_string
    call print_newline
    call exit
    
    .input_error:
        mov rdi, input_err_msg
        call print_error
        call exit
    
    .find_error:
	mov rdi, find_err_msg
        call print_error
        call exit
        

    
    
    
