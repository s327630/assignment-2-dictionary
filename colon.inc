%macro colon 2; arg 1 - key, arg 2 - label
	%define key %1
	%define label %2
	
	%ifstr key
		%ifid label
			%ifdef prev
				label: dq prev
			%else
				label: dq 0
			%endif
			db key, 0x0
			%xdefine prev label
		%else
			%error "arg 2 must be a label"
		%endif
	%else 
		%error "key must be a string"
	%endif
%endmacro
